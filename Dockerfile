FROM java:8

ENV TZ=America/Bogota
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

CMD ["mkdir", "/home/app/"]
COPY target/spring-soap-1.0.0.jar /home/app/

CMD chmod -R 777
CMD java -Djava.security.egd=file:/dev/urandom -jar /home/app/spring-soap-1.0.0.jar --spring.profiles.active=production

EXPOSE 9090